#define MYVERSION "0.24.12"

/*
	changelog

2015-03-15 00:48 UTC - kode54
- Updated vio2sf, which now includes band limited linear interpolation
  resampling mode; all existing modes remain the same, and we still
  default to cubic interpolation
- Version is now 0.24.12

2014-11-26 05:10 UTC - kode54
- Fixed parsing seconds/milliseconds only timestamps
- Version is now 0.24.11

2014-11-25 00:40 UTC - kode54
- Updated psflib to fix tag error handling
- Replaced timestamp parsing function
- Version is now 0.24.10

2014-04-05 03:27 UTC - kode54
- Updated the sinc resampler
- Version is now 0.24.9

2014-04-05 00:08 UTC - kode54
- Updated the resampler
- Version is now 0.24.8

2014-04-01 04:02 UTC - kode54
- Implemented multiple resampling modes, with a configuration option
- Version is now 0.24.7

2014-03-27 20:32 UTC - kode54
- Fixed seeking when silence test buffer is not empty
- Version is now 0.24.6

2014-02-27 00:30 UTC - kode54
- Now supports reading emulator setup tags from nested library files and
  not just the top level mini2sf
- Version is now 0.24.5

2013-10-16 03:56 UTC - kode54
- Fixed the PSG mode, which we now supersample through the Lanczos
  resampler which is currently enabled
- Version is now 0.24.4

2013-10-16 00:45 UTC - kode54
- Fixed tag writing (forgot to change the PSF version checking)
- Version is now 0.24.3

2013-10-14 05:15 UTC - kode54
- Updated psflib to hopefully parse over large exe chunks more quickly
- Version is now 0.24.2

2013-10-14 04:32 UTC - kode54
- Replaced cosine interpolation with Lanczos windowed FIR resampler
- Enabled Lanczos resampler by default
- Version is now 0.24.1

2013-10-14 03:27 UTC - kode54
- Added integer squarer root function
- Cleaned up more warnings

2013-10-14 02:56 UTC - kode54
- Fixed save state loading
- Cleaned up most of the warnings, including a few cases where operator
  precedence would have caused unintended behavior

2013-10-14 02:03 UTC - kode54
- vio2sf now tries to queue up more samples per loop
- End silence detection fill will now leave behind some remainder if it
  exceeds the free space in the silence detection buffer
- Enabled profile guided optimization

2013-10-13 21:58 UTC - kode54
- Completed
- Version is now 0.24

2013-10-13 20:30 UTC - kode54
- Copied from foo_input_gsf code base

*/

#define _WIN32_WINNT 0x0501

#include "../SDK/foobar2000.h"
#include "../helpers/window_placement_helper.h"
#include "../ATLHelpers/ATLHelpers.h"

#include "resource.h"

#include <stdio.h>

#include "../../../vio2sf/src/vio2sf/desmume/state.h"

#include <psflib.h>

#include "circular_buffer.h"

#include <atlbase.h>
#include <atlapp.h>
#include <atlwin.h>
#include <atlctrls.h>
#include <atlctrlx.h>

#include <zlib.h>

//#define DBG(a) OutputDebugString(a)
#define DBG(a)

typedef unsigned long u_long;

// {C494EC34-A83A-4624-924B-020ACA2D8DC8}
static const GUID guid_cfg_infinite = 
{ 0xc494ec34, 0xa83a, 0x4624, { 0x92, 0x4b, 0x2, 0xa, 0xca, 0x2d, 0x8d, 0xc8 } };
// {698012C1-2EC7-4695-B6C7-43EB04BE7193}
static const GUID guid_cfg_deflength = 
{ 0x698012c1, 0x2ec7, 0x4695, { 0xb6, 0xc7, 0x43, 0xeb, 0x4, 0xbe, 0x71, 0x93 } };
// {1E5D4FF6-B16D-404E-9C80-110845ECF989}
static const GUID guid_cfg_deffade = 
{ 0x1e5d4ff6, 0xb16d, 0x404e, { 0x9c, 0x80, 0x11, 0x8, 0x45, 0xec, 0xf9, 0x89 } };
// {ACDB11F9-0A8E-4A1D-93C9-781BAD1067F3}
static const GUID guid_cfg_suppressopeningsilence = 
{ 0xacdb11f9, 0xa8e, 0x4a1d, { 0x93, 0xc9, 0x78, 0x1b, 0xad, 0x10, 0x67, 0xf3 } };
// {3DC2BBC8-8A21-46EA-8630-04CCC8C8CEE1}
static const GUID guid_cfg_suppressendsilence = 
{ 0x3dc2bbc8, 0x8a21, 0x46ea, { 0x86, 0x30, 0x4, 0xcc, 0xc8, 0xc8, 0xce, 0xe1 } };
// {EB909A04-EA61-4396-AF61-1E0ABBD69A39}
static const GUID guid_cfg_endsilenceseconds = 
{ 0xeb909a04, 0xea61, 0x4396, { 0xaf, 0x61, 0x1e, 0xa, 0xbb, 0xd6, 0x9a, 0x39 } };
// {59CDC362-A08E-4A60-B95E-4E6A5A60F755}
static const GUID guid_cfg_placement = 
{ 0x59cdc362, 0xa08e, 0x4a60, { 0xb9, 0x5e, 0x4e, 0x6a, 0x5a, 0x60, 0xf7, 0x55 } };
// {FDF0BDBD-39CB-41EB-9AB7-4C608D59D4DB}
static const GUID guid_cfg_resampling_quality_v1 = 
{ 0xfdf0bdbd, 0x39cb, 0x41eb, { 0x9a, 0xb7, 0x4c, 0x60, 0x8d, 0x59, 0xd4, 0xdb } };
// {21A968B5-5CFC-4CA5-859F-34B57A9C60ED}
static const GUID guid_cfg_resampling_quality_v2 =
{ 0x21a968b5, 0x5cfc, 0x4ca5, { 0x85, 0x9f, 0x34, 0xb5, 0x7a, 0x9c, 0x60, 0xed } };

enum
{
	default_cfg_infinite = 0,
	default_cfg_deflength = 170000,
	default_cfg_deffade = 10000,
	default_cfg_suppressopeningsilence = 1,
	default_cfg_suppressendsilence = 1,
	default_cfg_endsilenceseconds = 5,
	default_cfg_resampling_quality = 4
};

static cfg_int cfg_infinite(guid_cfg_infinite,default_cfg_infinite);
static cfg_int cfg_deflength(guid_cfg_deflength,default_cfg_deflength);
static cfg_int cfg_deffade(guid_cfg_deffade,default_cfg_deffade);
static cfg_int cfg_suppressopeningsilence(guid_cfg_suppressopeningsilence,default_cfg_suppressopeningsilence);
static cfg_int cfg_suppressendsilence(guid_cfg_suppressendsilence,default_cfg_suppressendsilence);
static cfg_int cfg_endsilenceseconds(guid_cfg_endsilenceseconds,default_cfg_endsilenceseconds);
static cfg_int cfg_resampling_quality_v1(guid_cfg_resampling_quality_v1,-1);
static cfg_int cfg_resampling_quality(guid_cfg_resampling_quality_v2,default_cfg_resampling_quality);
static cfg_window_placement cfg_placement(guid_cfg_placement);

class initquit_fix_config : public initquit
{
public:
	void on_init()
	{
		if (cfg_resampling_quality_v1 != -1)
		{
			cfg_resampling_quality = cfg_resampling_quality_v1 + (cfg_resampling_quality_v1 >= 3);
			cfg_resampling_quality_v1 = -1;
		}
	}

	void on_quit() {}
};

static const char field_length[]="2sf_length";
static const char field_fade[]="2sf_fade";

#define BORK_TIME 0xC0CAC01A

static unsigned long parse_time_crap(const char *input)
{
	unsigned long value = 0;
	unsigned long multiplier = 1000;
	const char * ptr = input;
	unsigned long colon_count = 0;

	while (*ptr && ((*ptr >= '0' && *ptr <= '9') || *ptr == ':'))
	{
		colon_count += *ptr == ':';
		++ptr;
	}
	if (colon_count > 2) return BORK_TIME;
	if (*ptr && *ptr != '.' && *ptr != ',') return BORK_TIME;
	if (*ptr) ++ptr;
	while (*ptr && *ptr >= '0' && *ptr <= '9') ++ptr;
	if (*ptr) return BORK_TIME;

	ptr = strrchr(input, ':');
	if (!ptr)
		ptr = input;
	for (;;)
	{
		char * end;
		if (ptr != input) ++ptr;
		if (multiplier == 1000)
		{
			double temp = pfc::string_to_float(ptr);
			if (temp >= 60.0) return BORK_TIME;
			value = (long)(temp * 1000.0f);
		}
		else
		{
			unsigned long temp = strtoul(ptr, &end, 10);
			if (temp >= 60 && multiplier < 3600000) return BORK_TIME;
			value += temp * multiplier;
		}
		if (ptr == input) break;
		ptr -= 2;
		while (ptr > input && *ptr != ':') --ptr;
		multiplier *= 60;
	}

	return value;
}

static void print_time_crap(int ms, char *out)
{
	char frac[8];
	int i,h,m,s;
	if (ms % 1000)
	{
		sprintf(frac, ".%3.3d", ms % 1000);
		for (i = 3; i > 0; i--)
			if (frac[i] == '0') frac[i] = 0;
		if (!frac[1]) frac[0] = 0;
	}
	else
		frac[0] = 0;
	h = ms / (60*60*1000);
	m = (ms % (60*60*1000)) / (60*1000);
	s = (ms % (60*1000)) / 1000;
	if (h) sprintf(out, "%d:%2.2d:%2.2d%s",h,m,s,frac);
	else if (m) sprintf(out, "%d:%2.2d%s",m,s,frac);
	else sprintf(out, "%d%s",s,frac);
}

static void info_meta_add(file_info & info, const char * tag, pfc::ptr_list_t< const char > const& values)
{
	t_size count = info.meta_get_count_by_name( tag );
	if ( count )
	{
		// append as another line
		pfc::string8 final = info.meta_get(tag, count - 1);
		final += "\r\n";
		final += values[0];
		info.meta_modify_value( info.meta_find( tag ), count - 1, final );
	}
	else
	{
		info.meta_add(tag, values[0]);
	}
	for ( count = 1; count < values.get_count(); count++ )
	{
		info.meta_add( tag, values[count] );
	}
}

static void info_meta_ansi( file_info & info )
{
	for ( unsigned i = 0, j = info.meta_get_count(); i < j; i++ )
	{
		for ( unsigned k = 0, l = info.meta_enum_value_count( i ); k < l; k++ )
		{
			const char * value = info.meta_enum_value( i, k );
			info.meta_modify_value( i, k, pfc::stringcvt::string_utf8_from_ansi( value ) );
		}
	}
	for ( unsigned i = 0, j = info.info_get_count(); i < j; i++ )
	{
		const char * name = info.info_enum_name( i );
		if ( name[ 0 ] == '_' )
			info.info_set( pfc::string8( name ), pfc::stringcvt::string_utf8_from_ansi( info.info_enum_value( i ) ) );
	}
}

static int find_crlf(pfc::string8 & blah)
{
	int pos = blah.find_first('\r');
	if (pos >= 0 && *(blah.get_ptr()+pos+1) == '\n') return pos;
	return -1;
}

static const char * fields_to_split[] = {"ARTIST", "ALBUM ARTIST", "PRODUCER", "COMPOSER", "PERFORMER", "GENRE"};

static bool meta_split_value( const char * tag )
{
	for ( unsigned i = 0; i < _countof( fields_to_split ); i++ )
	{
		if ( !stricmp_utf8( tag, fields_to_split[ i ] ) ) return true;
	}
	return false;
}

static void info_meta_write(pfc::string_base & tag, const file_info & info, const char * name, int idx, int & first)
{
	pfc::string8 v = info.meta_enum_value(idx, 0);
	if (meta_split_value(name))
	{
		t_size count = info.meta_enum_value_count(idx);
		for (t_size i = 1; i < count; i++)
		{
			v += "; ";
			v += info.meta_enum_value(idx, i);
		}
	}

	int pos = find_crlf(v);

	if (pos == -1)
	{
		if (first) first = 0;
		else tag.add_byte('\n');
		tag += name;
		tag.add_byte('=');
		// r->write(v.c_str(), v.length());
		tag += v;
		return;
	}
	while (pos != -1)
	{
		pfc::string8 foo;
		foo = v;
		foo.truncate(pos);
		if (first) first = 0;
		else tag.add_byte('\n');
		tag += name;
		tag.add_byte('=');
		tag += foo;
		v = v.get_ptr() + pos + 2;
		pos = find_crlf(v);
	}
	if (v.length())
	{
		tag.add_byte('\n');
		tag += name;
		tag.add_byte('=');
		tag += v;
	}
}

struct psf_info_meta_state
{
	file_info * info;

	pfc::string8_fast name;

	bool utf8;

	int tag_song_ms;
	int tag_fade_ms;

	psf_info_meta_state()
		: info( 0 ), utf8( false ), tag_song_ms( 0 ), tag_fade_ms( 0 )
	{
	}
};

static int psf_info_meta(void * context, const char * name, const char * value)
{
	psf_info_meta_state * state = ( psf_info_meta_state * ) context;

	pfc::string8_fast & tag = state->name;

	tag = name;

	if (!stricmp_utf8(tag, "game"))
	{
		DBG("reading game as album");
		tag = "album";
	}
	else if (!stricmp_utf8(tag, "year"))
	{
		DBG("reading year as date");
		tag = "date";
	}

	if (!stricmp_utf8_partial(tag, "replaygain_"))
	{
		DBG("reading RG info");
		//info.info_set(tag, value);
		state->info->info_set_replaygain(tag, value);
	}
	else if (!stricmp_utf8(tag, "length"))
	{
		DBG("reading length");
		int temp = parse_time_crap(value);
		if (temp != BORK_TIME)
		{
			state->tag_song_ms = temp;
			state->info->info_set_int(field_length, state->tag_song_ms);
		}
	}
	else if (!stricmp_utf8(tag, "fade"))
	{
		DBG("reading fade");
		int temp = parse_time_crap(value);
		if (temp != BORK_TIME)
		{
			state->tag_fade_ms = temp;
			state->info->info_set_int(field_fade, state->tag_fade_ms);
		}
	}
	else if (!stricmp_utf8(tag, "utf8"))
	{
		state->utf8 = true;
	}
	else if (!stricmp_utf8_partial(tag, "_lib"))
	{
		DBG("found _lib");
		state->info->info_set(tag, value);
	}
	else if (!stricmp_utf8(tag, "_frames"))
	{
		DBG("found _frames");
		state->info->info_set(tag, value);
	}
	else if (!stricmp_utf8(tag, "_clockdown"))
	{
		DBG("found legacy _clockdown");
		state->info->info_set(tag, value);
	}
	else if (!stricmp_utf8(tag, "_vio2sf_sync_type"))
	{
		DBG("found vio2sf sync type");
		state->info->info_set(tag, value);
	}
	else if (!stricmp_utf8(tag, "_vio2sf_arm9_clockdown_level"))
	{
		DBG("found vio2sf arm9 clockdown");
		state->info->info_set(tag, value);
	}
	else if (!stricmp_utf8(tag, "_vio2sf_arm7_clockdown_level"))
	{
		DBG("found vio2sf arm7 clockdown");
		state->info->info_set(tag, value);
	}
	else if (tag[0] == '_')
	{
		DBG("found unknown required tag, failing");
		console::formatter() << "Unsupported tag found: " << tag << ", required to play file.";
		return -1;
	}
	else
	{
		state->info->meta_add( tag, value );
	}

	return 0;
}

inline unsigned get_le32( void const* p )
{
    return  (unsigned) ((unsigned char const*) p) [3] << 24 |
            (unsigned) ((unsigned char const*) p) [2] << 16 |
            (unsigned) ((unsigned char const*) p) [1] <<  8 |
            (unsigned) ((unsigned char const*) p) [0];
}

struct twosf_loader_state
{
	uint8_t * rom;
	uint8_t * state;
	size_t rom_size;
	size_t state_size;

	int initial_frames;
	int sync_type;
	int clockdown;
	int arm9_clockdown_level;
	int arm7_clockdown_level;

	twosf_loader_state()
		: rom(0), state(0), rom_size(0), state_size(0),
		initial_frames(-1), sync_type(0), clockdown(0),
		arm9_clockdown_level(0), arm7_clockdown_level(0)
	{
	}

	~twosf_loader_state()
	{
		if (rom) free(rom);
		if (state) free(state);
	}
};

static int load_twosf_map(struct twosf_loader_state *state, int issave, const unsigned char *udata, unsigned usize)
{
	if (usize < 8) return -1;

	unsigned char *iptr;
	size_t isize;
	unsigned char *xptr;
	unsigned xsize = get_le32(udata + 4);
	unsigned xofs = get_le32(udata + 0);
	if (issave)
	{
		iptr = state->state;
		isize = state->state_size;
		state->state = 0;
		state->state_size = 0;
	}
	else
	{
		iptr = state->rom;
		isize = state->rom_size;
		state->rom = 0;
		state->rom_size = 0;
	}
	if (!iptr)
	{
		size_t rsize = xofs + xsize;
		if (!issave)
		{
			rsize -= 1;
			rsize |= rsize >> 1;
			rsize |= rsize >> 2;
			rsize |= rsize >> 4;
			rsize |= rsize >> 8;
			rsize |= rsize >> 16;
			rsize += 1;
		}
		iptr = (unsigned char *) malloc(rsize + 10);
		if (!iptr)
			return -1;
		memset(iptr, 0, rsize + 10);
		isize = rsize;
	}
	else if (isize < xofs + xsize)
	{
		size_t rsize = xofs + xsize;
		if (!issave)
		{
			rsize -= 1;
			rsize |= rsize >> 1;
			rsize |= rsize >> 2;
			rsize |= rsize >> 4;
			rsize |= rsize >> 8;
			rsize |= rsize >> 16;
			rsize += 1;
		}
		xptr = (unsigned char *) realloc(iptr, xofs + rsize + 10);
		if (!xptr)
		{
			free(iptr);
			return -1;
		}
		iptr = xptr;
		isize = rsize;
	}
	memcpy(iptr + xofs, udata + 8, xsize);
	if (issave)
	{
		state->state = iptr;
		state->state_size = isize;
	}
	else
	{
		state->rom = iptr;
		state->rom_size = isize;
	}
	return 0;
}

static int load_twosf_mapz(struct twosf_loader_state *state, int issave, const unsigned char *zdata, unsigned zsize, unsigned zcrc)
{
	int ret;
	int zerr;
	uLongf usize = 8;
	uLongf rsize = usize;
	unsigned char *udata;
	unsigned char *rdata;

	udata = (unsigned char *) malloc(usize);
	if (!udata)
		return -1;

	while (Z_OK != (zerr = uncompress(udata, &usize, zdata, zsize)))
	{
		if (Z_MEM_ERROR != zerr && Z_BUF_ERROR != zerr)
		{
			free(udata);
			return -1;
		}
		if (usize >= 8)
		{
			usize = get_le32(udata + 4) + 8;
			if (usize < rsize)
			{
				rsize += rsize;
				usize = rsize;
			}
			else
				rsize = usize;
		}
		else
		{
			rsize += rsize;
			usize = rsize;
		}
		rdata = (unsigned char *) realloc(udata, usize);
		if (!rdata)
		{
			free(udata);
			return -1;
		}
		udata = rdata;
	}

	rdata = (unsigned char *) realloc(udata, usize);
	if (!rdata)
	{
		free(udata);
		return -1;
	}

	if (0)
	{
		uLong ccrc = crc32(crc32(0L, Z_NULL, 0), rdata, (uInt) usize);
		if (ccrc != zcrc)
			return -1;
	}

	ret = load_twosf_map(state, issave, rdata, (unsigned) usize);
	free(rdata);
	return ret;
}

static int twosf_loader(void * context, const uint8_t * exe, size_t exe_size,
						const uint8_t * reserved, size_t reserved_size)
{
	struct twosf_loader_state * state = ( struct twosf_loader_state * ) context;

	if ( exe_size >= 8 )
	{
		if ( load_twosf_map(state, 0, exe, (unsigned) exe_size) )
			return -1;
	}

	if ( reserved_size )
	{
		size_t resv_pos = 0;
		if ( reserved_size < 16 )
			return -1;
		while ( resv_pos + 12 < reserved_size )
		{
			unsigned save_size = get_le32(reserved + resv_pos + 4);
			unsigned save_crc = get_le32(reserved + resv_pos + 8);
			if (get_le32(reserved + resv_pos + 0) == 0x45564153)
			{
				if (resv_pos + 12 + save_size > reserved_size)
					return -1;
				if (load_twosf_mapz(state, 1, reserved + resv_pos + 12, save_size, save_crc))
					return -1;
			}
			resv_pos += 12 + save_size;
		}
	}

	return 0;
}

static int twosf_info(void * context, const char * name, const char * value)
{
	struct twosf_loader_state * state = ( struct twosf_loader_state * ) context;
	char *end;

	if ( !stricmp_utf8( name, "_frames" ) )
	{
		state->initial_frames = strtol( value, &end, 10 );
	}
	else if ( !stricmp_utf8( name, "_clockdown" ) )
	{
		state->clockdown = strtol( value, &end, 10 );
	}
	else if ( !stricmp_utf8( name, "_vio2sf_sync_type") )
	{
		state->sync_type = strtol( value, &end, 10 );
	}
	else if ( !stricmp_utf8( name, "_vio2sf_arm9_clockdown_level" ) )
	{
		state->arm9_clockdown_level = strtol( value, &end, 10 );
	}
	else if ( !stricmp_utf8( name, "_vio2sf_arm7_clockdown_level" ) )
	{
		state->arm7_clockdown_level = strtol( value, &end, 10 );
	}

	return 0;
}

static class psf_file_container
{
	critical_section lock;

	struct psf_file_opened
	{
		pfc::string_simple path;
		file::ptr f;

		psf_file_opened() { }

		psf_file_opened( const char * _p )
			: path( _p ) { }

		psf_file_opened( const char * _p, file::ptr _f )
			: path( _p ), f( _f ) { }

		bool operator== ( psf_file_opened const& in ) const
		{
			return !strcmp( path, in.path );
		}
	};

	pfc::list_t<psf_file_opened> hints;

public:
	void add_hint( const char * path, file::ptr f )
	{
		insync( lock );
		hints.add_item( psf_file_opened( path, f ) );
	}

	void remove_hint( const char * path )
	{
		insync( lock );
		hints.remove_item( psf_file_opened( path ) );
	}

	bool try_hint( const char * path, file::ptr & out )
	{
		insync( lock );
		t_size index = hints.find_item( psf_file_opened( path ) );
		if ( index == ~0 ) return false;
		out = hints[ index ].f;
		out->reopen( abort_callback_dummy() );
		return true;
	}
} g_hint_list;

struct psf_file_state
{
	file::ptr f;
};

static void * psf_file_fopen( const char * uri )
{
	try
	{
		psf_file_state * state = new psf_file_state;
		if ( !g_hint_list.try_hint( uri, state->f ) )
			filesystem::g_open( state->f, uri, filesystem::open_mode_read, abort_callback_dummy() );
		return state;
	}
	catch (...)
	{
		return NULL;
	}
}

static size_t psf_file_fread( void * buffer, size_t size, size_t count, void * handle )
{
	try
	{
		psf_file_state * state = ( psf_file_state * ) handle;
		size_t bytes_read = state->f->read( buffer, size * count, abort_callback_dummy() );
		return bytes_read / size;
	}
	catch (...)
	{
		return 0;
	}
}

static int psf_file_fseek( void * handle, int64_t offset, int whence )
{
	try
	{
		psf_file_state * state = ( psf_file_state * ) handle;
		state->f->seek_ex( offset, (foobar2000_io::file::t_seek_mode) whence, abort_callback_dummy() );
		return 0;
	}
	catch (...)
	{
		return -1;
	}
}

static int psf_file_fclose( void * handle )
{
	try
	{
		psf_file_state * state = ( psf_file_state * ) handle;
		delete state;
		return 0;
	}
	catch (...)
	{
		return -1;
	}
}

static long psf_file_ftell( void * handle )
{
	try
	{
		psf_file_state * state = ( psf_file_state * ) handle;
		return state->f->get_position( abort_callback_dummy() );
	}
	catch (...)
	{
		return -1;
	}
}

const psf_file_callbacks psf_file_system =
{
	"\\/|:",
	psf_file_fopen,
	psf_file_fread,
	psf_file_fseek,
	psf_file_fclose,
	psf_file_ftell
};

class input_twosf
{
	bool no_loop, eof;

	circular_buffer<t_int16> silence_test_buffer;
	pfc::array_t<t_int16> sample_buffer;

	twosf_loader_state m_state;

	NDS_state *m_emu;

	service_ptr_t<file> m_file;

	pfc::string8 m_path;

	int err;

	int data_written,remainder,pos_delta,startsilence,silence;

	double twosfemu_pos;

	int song_len,fade_len;
	int tag_song_ms,tag_fade_ms;

	file_info_impl m_info;

	bool do_filter, do_suppressendsilence;

public:
	input_twosf() : silence_test_buffer( 0 ), m_emu( 0 )
	{
	}

	~input_twosf()
	{
		g_hint_list.remove_hint( m_path );

		shutdown();
	}

	void shutdown()
	{
		if ( m_emu )
		{
			state_deinit( m_emu );
			free( m_emu );
			m_emu = NULL;
		}
	}

	void open( service_ptr_t<file> p_file, const char * p_path, t_input_open_reason p_reason, abort_callback & p_abort )
	{
		input_open_file_helper( p_file, p_path, p_reason, p_abort );

		m_path = p_path;
		g_hint_list.add_hint( p_path, p_file );

		psf_info_meta_state info_state;
		info_state.info = &m_info;

		if ( psf_load( p_path, &psf_file_system, 0x24, 0, 0, psf_info_meta, &info_state, 0 ) <= 0 )
			throw exception_io_data( "Not a 2SF file" );

		if ( !info_state.utf8 )
			info_meta_ansi( m_info );

		tag_song_ms = info_state.tag_song_ms;
		tag_fade_ms = info_state.tag_fade_ms;

		if (!tag_song_ms)
		{
			tag_song_ms = cfg_deflength;
			tag_fade_ms = cfg_deffade;
		}

		m_info.set_length( (double)( tag_song_ms + tag_fade_ms ) * .001 );
		m_info.info_set_int( "samplerate", 44100 );
		m_info.info_set_int( "channels", 2 );

		m_file = p_file;
	}

	void get_info( file_info & p_info, abort_callback & p_abort )
	{
		p_info.copy( m_info );
	}

	t_filestats get_file_stats( abort_callback & p_abort )
	{
		return m_file->get_stats( p_abort );
	}

	void decode_initialize( unsigned p_flags, abort_callback & p_abort )
	{
		shutdown();

		m_emu = ( NDS_state * ) calloc( 1, sizeof(NDS_state) );
		if ( !m_emu )
			throw std::bad_alloc();

		if ( state_init( m_emu ) )
			throw std::bad_alloc();

		if ( !m_state.rom && !m_state.state )
		{
			if ( psf_load( m_path, &psf_file_system, 0x24, twosf_loader, &m_state, twosf_info, &m_state, 1 ) < 0 )
				throw exception_io_data( "Invalid 2SF" );

			if (!m_state.arm7_clockdown_level)
				m_state.arm7_clockdown_level = m_state.clockdown;
			if (!m_state.arm9_clockdown_level)
				m_state.arm9_clockdown_level = m_state.clockdown;
		}

		m_emu->dwInterpolation = cfg_resampling_quality;
		m_emu->dwChannelMute = 0;

		m_emu->initial_frames = m_state.initial_frames;
		m_emu->sync_type = m_state.sync_type;
		m_emu->arm7_clockdown_level = m_state.arm7_clockdown_level;
		m_emu->arm9_clockdown_level = m_state.arm9_clockdown_level;

		if ( m_state.rom )
			state_setrom( m_emu, m_state.rom, m_state.rom_size, 0 );

		state_loadstate( m_emu, m_state.state, m_state.state_size );

		twosfemu_pos = 0.;

		startsilence = silence = 0;

		eof = 0;
		err = 0;
		data_written = 0;
		remainder = 0;
		pos_delta = 0;
		no_loop = ( p_flags & input_flag_no_looping ) || !cfg_infinite;

		calcfade();

		do_suppressendsilence = !! cfg_suppressendsilence;

		sample_buffer.grow_size( 2048 );

		unsigned skip_max = cfg_endsilenceseconds * 44100;

		if ( cfg_suppressopeningsilence ) // ohcrap
		{

			for (;;)
			{
				p_abort.check();

				unsigned skip_howmany = skip_max - silence;
				if (skip_howmany > 1024) skip_howmany = 1024;
				state_render(m_emu, sample_buffer.get_ptr(), skip_howmany);
				short * foo = ( short * ) sample_buffer.get_ptr();
				unsigned i;
				for ( i = 0; i < skip_howmany; ++i )
				{
					if ( foo[ 0 ] || foo[ 1 ] ) break;
					foo += 2;
				}
				silence += i;
				if ( i < skip_howmany )
				{
					remainder = skip_howmany - i;
					memmove( sample_buffer.get_ptr(), foo, remainder * sizeof( short ) * 2 );
					break;
				}
				if ( silence >= skip_max )
				{
					eof = true;
					break;
				}
			}

			startsilence += silence;
			silence = 0;
		}

		if ( do_suppressendsilence ) silence_test_buffer.resize( skip_max * 2 );
	}

	bool decode_run( audio_chunk & p_chunk, abort_callback & p_abort )
	{
		p_abort.check();

		if ( ( eof || err < 0 ) && !silence_test_buffer.data_available() ) return false;

		if ( no_loop && tag_song_ms && ( pos_delta + MulDiv( data_written, 1000, 44100 ) ) >= tag_song_ms + tag_fade_ms )
			return false;

		UINT written = 0;

		int samples;

		if ( no_loop )
		{
			samples = ( song_len + fade_len ) - data_written;
			if ( samples > 1024 ) samples = 1024;
		}
		else
		{
			samples = 1024;
		}

		short * ptr;

		if ( do_suppressendsilence )
		{
			if ( !eof )
			{
				unsigned free_space = silence_test_buffer.free_space() / 2;
				while ( free_space )
				{
					p_abort.check();

					unsigned samples_to_render;
					if ( remainder )
					{
						samples_to_render = remainder;
						if (samples_to_render > free_space)
							samples_to_render = free_space;
						remainder -= samples_to_render;
					}
					else
					{
						samples_to_render = free_space;
						if (samples_to_render > 1024) samples_to_render = 1024;
						state_render( m_emu, sample_buffer.get_ptr(), samples_to_render );
					}
					silence_test_buffer.write( (short *) sample_buffer.get_ptr(), samples_to_render * 2 );
					free_space -= samples_to_render;
					if ( remainder )
					{
						memmove( sample_buffer.get_ptr(), sample_buffer.get_ptr() + samples_to_render * 2, remainder * sizeof(short) * 2 );
					}
				}
			}

			if ( silence_test_buffer.test_silence() )
			{
				eof = true;
				return false;
			}

			written = silence_test_buffer.data_available() / 2;
			if ( written > samples ) written = samples;
			sample_buffer.grow_size( ( written + remainder ) * 2 );
			silence_test_buffer.read( sample_buffer.get_ptr() + remainder * 2, written * 2 );
			ptr = sample_buffer.get_ptr() + remainder * 2;
		}
		else
		{
			if ( remainder )
			{
				written = remainder;
				remainder = 0;
			}
			else
			{
				written = samples;
				state_render( m_emu, sample_buffer.get_ptr(), written );
			}

			ptr = (short *) sample_buffer.get_ptr();
		}

		twosfemu_pos += double( written ) / 44100.;

		int d_start, d_end;
		d_start = data_written;
		data_written += written;
		d_end = data_written;

		if ( tag_song_ms && d_end > song_len && no_loop )
		{
			short * foo = sample_buffer.get_ptr();
			int n;
			for( n = d_start; n < d_end; ++n )
			{
				if ( n > song_len )
				{
					if ( n > song_len + fade_len )
					{
						* ( DWORD * ) foo = 0;
					}
					else
					{
						int bleh = song_len + fade_len - n;
						foo[ 0 ] = MulDiv( foo[ 0 ], bleh, fade_len );
						foo[ 1 ] = MulDiv( foo[ 1 ], bleh, fade_len );
					}
				}
				foo += 2;
			}
		}

		p_chunk.set_data_fixedpoint( ptr, written * 4, 44100, 2, 16, audio_chunk::channel_config_stereo );

		return true;
	}

	void decode_seek( double p_seconds, abort_callback & p_abort )
	{
		eof = false;

		double buffered_time = (double)(silence_test_buffer.data_available() / 2) / 44100.0;

		twosfemu_pos += buffered_time;

		silence_test_buffer.reset();

		if ( p_seconds < twosfemu_pos )
		{
			decode_initialize( no_loop ? input_flag_no_looping : 0, p_abort );
		}
		unsigned int howmany = ( int )( audio_math::time_to_samples( p_seconds - twosfemu_pos, 44100 ) );

		if ( howmany <= remainder )
		{
			remainder -= howmany;
			memmove( sample_buffer.get_ptr(), sample_buffer.get_ptr() + howmany * 2, remainder * sizeof(short) * 2 );
			howmany = 0;
		}
		else if ( remainder )
		{
			howmany -= remainder;
			remainder = 0;
		}

		// more abortable, and emu doesn't like doing huge numbers of samples per call anyway
		while ( howmany )
		{
			p_abort.check();

			unsigned int samples = (howmany > 1024) ? 1024 : howmany;

			state_render( m_emu, sample_buffer.get_ptr(), samples );

			howmany -= samples;
		}

		data_written = 0;
		pos_delta = ( int )( p_seconds * 1000. );
		twosfemu_pos = p_seconds;

		calcfade();
	}

	bool decode_can_seek()
	{
		return true;
	}

	bool decode_get_dynamic_info( file_info & p_out, double & p_timestamp_delta )
	{
		return false;
	}

	bool decode_get_dynamic_info_track( file_info & p_out, double & p_timestamp_delta )
	{
		return false;
	}

	void decode_on_idle( abort_callback & p_abort )
	{
	}

	void retag( const file_info & p_info, abort_callback & p_abort )
	{
		m_info.copy( p_info );

		pfc::array_t<t_uint8> buffer;
		buffer.set_size( 16 );

		m_file->seek( 0, p_abort );

		BYTE *ptr = buffer.get_ptr();
		m_file->read_object( ptr, 16, p_abort );
		if (ptr[0] != 'P' || ptr[1] != 'S' || ptr[2] != 'F' ||
			ptr[3] != 0x24) throw exception_io_data();
		int reserved_size = pfc::byteswap_if_be_t( ((unsigned long*)ptr)[1] );
		int exe_size = pfc::byteswap_if_be_t( ((unsigned long*)ptr)[2] );
		m_file->seek(16 + reserved_size + exe_size, p_abort);
		m_file->set_eof(p_abort);

		pfc::string8 tag = "[TAG]utf8=1\n";

		int first = 1;
		// _lib and _refresh tags first
		int n, p = p_info.info_get_count();
		for (n = 0; n < p; n++)
		{
			const char *t = p_info.info_enum_name(n);
			if (*t == '_')
			{
				if (first) first = 0;
				else tag.add_byte('\n');
				tag += t;
				tag.add_byte('=');
				tag += p_info.info_enum_value(n);
			}
		}
		// Then info
		p = p_info.meta_get_count();
		for (n = 0; n < p; n++)
		{
			const char * t = p_info.meta_enum_name(n);
			if (*t == '_' ||
				!stricmp(t, "length") ||
				!stricmp(t, "fade")) continue; // dummy protection
			if (!stricmp(t, "album")) info_meta_write(tag, p_info, "game", n, first);
			else if (!stricmp(t, "date"))
			{
				const char * val = p_info.meta_enum_value(n, 0);
				char * end;
				strtoul(p_info.meta_enum_value(n, 0), &end, 10);
				if (size_t(end - val) < strlen(val))
					info_meta_write(tag, p_info, t, n, first);
				else
					info_meta_write(tag, p_info, "year", n, first);
			}
			else info_meta_write(tag, p_info, t, n, first);
		}
		// Then time and fade
		{
			int tag_song_ms = 0, tag_fade_ms = 0;
			const char *t = p_info.info_get(field_length);
			if (t)
			{
				char temp[16];
				tag_song_ms = atoi(t);
				if (first) first = 0;
				else tag.add_byte('\n');
				tag += "length=";
				print_time_crap(tag_song_ms, temp);
				tag += temp;
				t = p_info.info_get(field_fade);
				if (t)
				{
					tag_fade_ms = atoi(t);
					tag.add_byte('\n');
					tag += "fade=";
					print_time_crap(tag_fade_ms, (char *)&temp);
					tag += temp;
				}
			}
		}

		// Then ReplayGain
		/*
		p = p_info.info_get_count();
		for (n = 0; n < p; n++)
		{
			const char *t = p_info.info_enum_name(n);
			if (!strnicmp(t, "replaygain_",11))
			{
				if (first) first = 0;
				else tag.add_byte('\n');
				tag += t;
				else tag.add_byte('=');
				tag += p_info.info_enum_value(n);
			}
		}
		*/
		replaygain_info rg = p_info.get_replaygain();
		char rgbuf[replaygain_info::text_buffer_size];
		if (rg.is_track_gain_present())
		{
			rg.format_track_gain(rgbuf);
			if (first) first = 0;
			else tag.add_byte('\n');
			tag += "replaygain_track_gain";
			tag.add_byte('=');
			tag += rgbuf;
		}
		if (rg.is_track_peak_present())
		{
			rg.format_track_peak(rgbuf);
			if (first) first = 0;
			else tag.add_byte('\n');
			tag += "replaygain_track_peak";
			tag.add_byte('=');
			tag += rgbuf;
		}
		if (rg.is_album_gain_present())
		{
			rg.format_album_gain(rgbuf);
			if (first) first = 0;
			else tag.add_byte('\n');
			tag += "replaygain_album_gain";
			tag.add_byte('=');
			tag += rgbuf;
		}
		if (rg.is_album_peak_present())
		{
			rg.format_album_peak(rgbuf);
			if (first) first = 0;
			else tag.add_byte('\n');
			tag += "replaygain_album_peak";
			tag.add_byte('=');
			tag += rgbuf;
		}

		m_file->write_object( tag.get_ptr(), tag.length(), p_abort );
	}

	static bool g_is_our_content_type( const char * p_content_type )
	{
		return false;
	}

	static bool g_is_our_path( const char * p_full_path, const char * p_extension )
	{
		return (!stricmp(p_extension,"2sf") || !stricmp(p_extension,"mini2sf"));
	}

private:
	void calcfade()
	{
		song_len=MulDiv(tag_song_ms-pos_delta,44100,1000);
		fade_len=MulDiv(tag_fade_ms,44100,1000);
	}
};

class CMyPreferences : public CDialogImpl<CMyPreferences>, public preferences_page_instance {
public:
	//Constructor - invoked by preferences_page_impl helpers - don't do Create() in here, preferences_page_impl does this for us
	CMyPreferences(preferences_page_callback::ptr callback) : m_callback(callback) {}

	//Note that we don't bother doing anything regarding destruction of our class.
	//The host ensures that our dialog is destroyed first, then the last reference to our preferences_page_instance object is released, causing our object to be deleted.


	//dialog resource ID
	enum {IDD = IDD_PSF_CONFIG};
	// preferences_page_instance methods (not all of them - get_wnd() is supplied by preferences_page_impl helpers)
	t_uint32 get_state();
	void apply();
	void reset();

	//WTL message map
	BEGIN_MSG_MAP(CMyPreferences)
		MSG_WM_INITDIALOG(OnInitDialog)
		COMMAND_HANDLER_EX(IDC_INDEFINITE, BN_CLICKED, OnButtonClick)
		COMMAND_HANDLER_EX(IDC_SOS, BN_CLICKED, OnButtonClick)
		COMMAND_HANDLER_EX(IDC_SES, BN_CLICKED, OnButtonClick)
		COMMAND_HANDLER_EX(IDC_RESAMPLING_QUALITY, CBN_SELCHANGE, OnSelectionChange)
		COMMAND_HANDLER_EX(IDC_SILENCE, EN_CHANGE, OnEditChange)
		COMMAND_HANDLER_EX(IDC_DLENGTH, EN_CHANGE, OnEditChange)
		COMMAND_HANDLER_EX(IDC_DFADE, EN_CHANGE, OnEditChange)
	END_MSG_MAP()
private:
	BOOL OnInitDialog(CWindow, LPARAM);
	void OnEditChange(UINT, int, CWindow);
	void OnSelectionChange(UINT, int, CWindow);
	void OnButtonClick(UINT, int, CWindow);
	bool HasChanged();
	void OnChanged();

	const preferences_page_callback::ptr m_callback;

	CHyperLink m_link_bitbucket;
	CHyperLink m_link_kode54;
};

BOOL CMyPreferences::OnInitDialog(CWindow, LPARAM) {
	SendDlgItemMessage( IDC_INDEFINITE, BM_SETCHECK, cfg_infinite );
	SendDlgItemMessage( IDC_SOS, BM_SETCHECK, cfg_suppressopeningsilence );
	SendDlgItemMessage( IDC_SES, BM_SETCHECK, cfg_suppressendsilence );
	
	SetDlgItemInt( IDC_SILENCE, cfg_endsilenceseconds, FALSE );

	CWindow w = GetDlgItem(IDC_RESAMPLING_QUALITY);
	uSendMessageText(w, CB_ADDSTRING, 0, "zero order hold");
	uSendMessageText(w, CB_ADDSTRING, 0, "blep synthesis");
	uSendMessageText(w, CB_ADDSTRING, 0, "linear");
	uSendMessageText(w, CB_ADDSTRING, 0, "blam synthesis");
	uSendMessageText(w, CB_ADDSTRING, 0, "cubic");
	uSendMessageText(w, CB_ADDSTRING, 0, "sinc");
	::SendMessage(w, CB_SETCURSEL, cfg_resampling_quality, 0);
	
	{
		char temp[16];
		// wsprintf((char *)&temp, "= %d Hz", 33868800 / cfg_divider);
		// SetDlgItemText(wnd, IDC_HZ, (char *)&temp);
		
		print_time_crap( cfg_deflength, (char *)&temp );
		uSetDlgItemText( m_hWnd, IDC_DLENGTH, (char *)&temp );
		
		print_time_crap( cfg_deffade, (char *)&temp );
		uSetDlgItemText( m_hWnd, IDC_DFADE, (char *)&temp );
	}
	
	m_link_bitbucket.SetLabel( _T( "vio2sf bitbucket page" ) );
	m_link_bitbucket.SetHyperLink( _T( "https://bitbucket.org/kode54/vio2sf" ) );
	m_link_bitbucket.SubclassWindow( GetDlgItem( IDC_URL ) );
	
	m_link_kode54.SetLabel( _T( "kode's foobar2000 plug-ins" ) );
	m_link_kode54.SetHyperLink( _T( "http://kode54.foobar2000.org/" ) );
	m_link_kode54.SubclassWindow( GetDlgItem( IDC_K54 ) );
	
	{
		/*OSVERSIONINFO ovi = { 0 };
		ovi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		BOOL bRet = ::GetVersionEx(&ovi);
		if ( bRet && ( ovi.dwMajorVersion >= 5 ) )*/
		{
			DWORD color = GetSysColor( 26 /* COLOR_HOTLIGHT */ );
			m_link_bitbucket.m_clrLink = color;
			m_link_bitbucket.m_clrVisited = color;
			m_link_kode54.m_clrLink = color;
			m_link_kode54.m_clrVisited = color;
		}
	}
	
	return FALSE;
}

void CMyPreferences::OnEditChange(UINT, int, CWindow) {
	OnChanged();
}

void CMyPreferences::OnButtonClick(UINT, int, CWindow) {
	OnChanged();
}

void CMyPreferences::OnSelectionChange(UINT, int, CWindow) {
	OnChanged();
}

t_uint32 CMyPreferences::get_state() {
	t_uint32 state = preferences_state::resettable;
	if (HasChanged()) state |= preferences_state::changed;
	return state;
}

void CMyPreferences::reset() {
	char temp[16];
	SendDlgItemMessage( IDC_INDEFINITE, BM_SETCHECK, default_cfg_infinite );
	SendDlgItemMessage( IDC_SOS, BM_SETCHECK, default_cfg_suppressopeningsilence );
	SendDlgItemMessage( IDC_SES, BM_SETCHECK, default_cfg_suppressendsilence );
	SendDlgItemMessage( IDC_RESAMPLING_QUALITY, CB_SETCURSEL, default_cfg_resampling_quality );
	SetDlgItemInt( IDC_SILENCE, default_cfg_endsilenceseconds, FALSE );
	print_time_crap( default_cfg_deflength, (char *)&temp );
	uSetDlgItemText( m_hWnd, IDC_DLENGTH, (char *)&temp );
	print_time_crap( default_cfg_deffade, (char *)&temp );
	uSetDlgItemText( m_hWnd, IDC_DFADE, (char *)&temp );
	
	OnChanged();
}

void CMyPreferences::apply() {
	int t;
	char temp[16];
	cfg_infinite = SendDlgItemMessage( IDC_INDEFINITE, BM_GETCHECK );
	cfg_suppressopeningsilence = SendDlgItemMessage( IDC_SOS, BM_GETCHECK );
	cfg_suppressendsilence = SendDlgItemMessage( IDC_SES, BM_GETCHECK );
	cfg_resampling_quality = SendDlgItemMessage( IDC_RESAMPLING_QUALITY, CB_GETCURSEL );
	t = GetDlgItemInt( IDC_SILENCE, NULL, FALSE );
	if ( t > 0 ) cfg_endsilenceseconds = t;
	SetDlgItemInt( IDC_SILENCE, cfg_endsilenceseconds, FALSE );
	t = parse_time_crap( string_utf8_from_window( GetDlgItem( IDC_DLENGTH ) ) );
	if ( t != BORK_TIME ) cfg_deflength = t;
	else
	{
		print_time_crap( cfg_deflength, (char *)&temp );
		uSetDlgItemText( m_hWnd, IDC_DLENGTH, (char *)&temp );
	}
	t = parse_time_crap( string_utf8_from_window( GetDlgItem( IDC_DFADE ) ) );
	if ( t != BORK_TIME ) cfg_deffade = t;
	else
	{
		print_time_crap( cfg_deffade, (char *)&temp );
		uSetDlgItemText( m_hWnd, IDC_DFADE, (char *)&temp );
	}
	
	OnChanged(); //our dialog content has not changed but the flags have - our currently shown values now match the settings so the apply button can be disabled
}

bool CMyPreferences::HasChanged() {
	//returns whether our dialog content is different from the current configuration (whether the apply button should be enabled or not)
	bool changed = false;
	if ( !changed && SendDlgItemMessage( IDC_INDEFINITE, BM_GETCHECK ) != cfg_infinite ) changed = true;
	if ( !changed && SendDlgItemMessage( IDC_SOS, BM_GETCHECK ) != cfg_suppressopeningsilence ) changed = true;
	if ( !changed && SendDlgItemMessage( IDC_SES, BM_GETCHECK ) != cfg_suppressendsilence ) changed = true;
	if ( !changed && SendDlgItemMessage( IDC_RESAMPLING_QUALITY, CB_GETCURSEL ) != cfg_resampling_quality ) changed = true;
	if ( !changed && GetDlgItemInt( IDC_SILENCE, NULL, FALSE ) != cfg_endsilenceseconds ) changed = true;
	if ( !changed )
	{
		int t = parse_time_crap( string_utf8_from_window( GetDlgItem( IDC_DLENGTH ) ) );
		if ( t != BORK_TIME && t != cfg_deflength ) changed = true;
	}
	if ( !changed )
	{
		int t = parse_time_crap( string_utf8_from_window( GetDlgItem( IDC_DFADE ) ) );
		if ( t != BORK_TIME && t != cfg_deffade ) changed = true;
	}
	return changed;
}
void CMyPreferences::OnChanged() {
	//tell the host that our state has changed to enable/disable the apply button appropriately.
	m_callback->on_state_changed();
}

class preferences_page_myimpl : public preferences_page_impl<CMyPreferences> {
	// preferences_page_impl<> helper deals with instantiation of our dialog; inherits from preferences_page_v3.
public:
	const char * get_name() {return "2SF Decoder";}
	GUID get_guid() {
		// {B4B5E0E0-8921-4EDC-A3FC-FA4B5F6456C0}
		static const GUID guid = { 0xb4b5e0e0, 0x8921, 0x4edc, { 0xa3, 0xfc, 0xfa, 0x4b, 0x5f, 0x64, 0x56, 0xc0 } };
		return guid;
	}
	GUID get_parent_guid() {return guid_input;}
};

typedef struct
{
	unsigned song, fade;
} INFOSTRUCT;

static INT_PTR CALLBACK TimeProc(HWND wnd,UINT msg,WPARAM wp,LPARAM lp)
{
	switch(msg)
	{
	case WM_INITDIALOG:
		uSetWindowLong(wnd,DWL_USER,lp);
		{
			INFOSTRUCT * i=(INFOSTRUCT*)lp;
			char temp[16];
			if (!i->song && !i->fade) uSetWindowText(wnd, "Set length");
			else uSetWindowText(wnd, "Edit length");
			if ( i->song != ~0 )
			{
				print_time_crap(i->song, (char*)&temp);
				uSetDlgItemText(wnd, IDC_LENGTH, (char*)&temp);
			}
			if ( i->fade != ~0 )
			{
				print_time_crap(i->fade, (char*)&temp);
				uSetDlgItemText(wnd, IDC_FADE, (char*)&temp);
			}
		}
		cfg_placement.on_window_creation(wnd);
		return 1;
	case WM_COMMAND:
		switch(wp)
		{
		case IDOK:
			{
				INFOSTRUCT * i=(INFOSTRUCT*)uGetWindowLong(wnd,DWL_USER);
				int foo;
				foo = parse_time_crap(string_utf8_from_window(wnd, IDC_LENGTH));
				if (foo != BORK_TIME) i->song = foo;
				else i->song = ~0;
				foo = parse_time_crap(string_utf8_from_window(wnd, IDC_FADE));
				if (foo != BORK_TIME) i->fade = foo;
				else i->fade = ~0;
			}
			EndDialog(wnd,1);
			break;
		case IDCANCEL:
			EndDialog(wnd,0);
			break;
		}
		break;
	case WM_DESTROY:
		cfg_placement.on_window_destruction(wnd);
		break;
	}
	return 0;
}

static bool context_time_dialog(unsigned * song_ms, unsigned * fade_ms)
{
	bool ret;
	INFOSTRUCT * i = new INFOSTRUCT;
	if (!i) return 0;
	i->song = *song_ms;
	i->fade = *fade_ms;
	HWND hwnd = core_api::get_main_window();
	ret = uDialogBox(IDD_TIME, hwnd, TimeProc, (long)i) > 0;
	if (ret)
	{
		*song_ms = i->song;
		*fade_ms = i->fade;
	}
	delete i;
	return ret;
}

class length_info_filter : public file_info_filter
{
	bool set_length, set_fade;
	unsigned m_length, m_fade;

	metadb_handle_list m_handles;

public:
	length_info_filter( const pfc::list_base_const_t<metadb_handle_ptr> & p_list )
	{
		set_length = false;
		set_fade = false;

		pfc::array_t<t_size> order;
		order.set_size(p_list.get_count());
		order_helper::g_fill(order.get_ptr(),order.get_size());
		p_list.sort_get_permutation_t(pfc::compare_t<metadb_handle_ptr,metadb_handle_ptr>,order.get_ptr());
		m_handles.set_count(order.get_size());
		for(t_size n = 0; n < order.get_size(); n++) {
			m_handles[n] = p_list[order[n]];
		}

	}

	void length( unsigned p_length )
	{
		set_length = true;
		m_length = p_length;
	}

	void fade( unsigned p_fade )
	{
		set_fade = true;
		m_fade = p_fade;
	}

	virtual bool apply_filter(metadb_handle_ptr p_location,t_filestats p_stats,file_info & p_info)
	{
		t_size index;
		if (m_handles.bsearch_t(pfc::compare_t<metadb_handle_ptr,metadb_handle_ptr>,p_location,index))
		{
			if ( set_length )
			{
				if ( m_length ) p_info.info_set_int( field_length, m_length );
				else p_info.info_remove( field_length );
			}
			if ( set_fade )
			{
				if ( m_fade ) p_info.info_set_int( field_fade, m_fade );
				else p_info.info_remove( field_fade );
			}
			return set_length | set_fade;
		}
		else
		{
			return false;
		}
	}
};

class context_twosf : public contextmenu_item_simple
{
public:
	virtual unsigned get_num_items() { return 1; }

	virtual void get_item_name(unsigned n, pfc::string_base & out)
	{
		if (n) uBugCheck();
		out = "Edit length";
	}

	/*virtual void get_item_default_path(unsigned n, pfc::string_base & out)
	{
		out.reset();
	}*/
	GUID get_parent() {return contextmenu_groups::tagging;}

	virtual bool get_item_description(unsigned n, pfc::string_base & out)
	{
		if (n) uBugCheck();
		out = "Edits the length of the selected 2SF file, or sets the length of all selected 2SF files.";
		return true;
	}

	virtual GUID get_item_guid(unsigned p_index)
	{
		if (p_index) uBugCheck();
		static const GUID guid = { 0x3fe38c5a, 0xa7e5, 0x4e13, { 0xbf, 0x95, 0x3d, 0x59, 0x2f, 0xca, 0x5d, 0x68 } };
		return guid;
	}

	virtual bool context_get_display(unsigned n,const pfc::list_base_const_t<metadb_handle_ptr> & data,pfc::string_base & out,unsigned & displayflags,const GUID &)
	{
		if (n) uBugCheck();
		unsigned i, j;
		i = data.get_count();
		for (j = 0; j < i; j++)
		{
			pfc::string_extension ext(data.get_item(j)->get_path());
			if (stricmp_utf8(ext, "2SF") && stricmp_utf8(ext, "MINI2SF")) return false;
		}
		if (i == 1) out = "Edit length";
		else out = "Set length";
		return true;
	}

	virtual void context_command(unsigned n,const pfc::list_base_const_t<metadb_handle_ptr> & data,const GUID& caller)
	{
		if (n) uBugCheck();
		unsigned tag_song_ms = 0, tag_fade_ms = 0;
		unsigned i = data.get_count();
		file_info_impl info;
		abort_callback_impl m_abort;
		if (i == 1)
		{
			// fetch info from single file
			metadb_handle_ptr handle = data.get_item(0);
			handle->metadb_lock();
			const file_info * p_info;
			if (handle->get_info_locked(p_info) && p_info)
			{
				const char *t = p_info->info_get(field_length);
				if (t) tag_song_ms = atoi(t);
				t = p_info->info_get(field_fade);
				if (t) tag_fade_ms = atoi(t);
			}
			handle->metadb_unlock();
		}
		if (!context_time_dialog(&tag_song_ms, &tag_fade_ms)) return;
		static_api_ptr_t<metadb_io_v2> p_imgr;

		service_ptr_t<length_info_filter> p_filter = new service_impl_t< length_info_filter >( data );
		if ( tag_song_ms != ~0 ) p_filter->length( tag_song_ms );
		if ( tag_fade_ms != ~0 ) p_filter->fade( tag_fade_ms );

		p_imgr->update_info_async( data, p_filter, core_api::get_main_window(), 0, 0 );
	}
};

class version_twosf : public componentversion
{
public:
	virtual void get_file_name(pfc::string_base & out) { out = core_api::get_my_file_name(); }
	virtual void get_component_name(pfc::string_base & out) { out = "2SF Decoder"; }
	virtual void get_component_version(pfc::string_base & out) { out = MYVERSION; }
	virtual void get_about_message(pfc::string_base & out)
	{
		out = "Foobar2000 version by kode54\n\n"
			"DeSmuME v0.8.0\n"
			"Copyright (C) 2006 yopyop\n"
			"Copyright (C) 2006-2007 DeSmuME team\n"
			"\nhttps://bitbucket.org/kode54/vio2sf\nhttp://kode54.foobar2000.org/";
	}
};

DECLARE_FILE_TYPE( "2SF files", "*.2SF;*.MINI2SF" );

static input_singletrack_factory_t<input_twosf>             g_input_twosf_factory;
static preferences_page_factory_t <preferences_page_myimpl> g_config_twosf_factory;
static contextmenu_item_factory_t <context_twosf>           g_contextmenu_item_twosf_factory;
static service_factory_single_t   <version_twosf>           g_componentversion_twosf_factory;

VALIDATE_COMPONENT_FILENAME("foo_input_vio2sf.dll");
